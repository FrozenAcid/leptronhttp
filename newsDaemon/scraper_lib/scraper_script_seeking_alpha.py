import dryscrape
from bs4 import BeautifulSoup
import sys
from PyQt5.QtGui import *  
from PyQt5.QtCore import *  
from PyQt5.QtWebKit import *
from PyQt5.QtWebKitWidgets import QWebPage
from PyQt5.QtWidgets import QApplication
import os
import lxml
import json

os.environ['QT_QPA_PLATFORM']='offscreen'

ticker = sys.argv[1]
seeking_alpha_url = "https://seekingalpha.com/symbol/" + ticker

class Render(QWebPage):
    def __init__(self, url):
        self.app = QApplication(sys.argv)  
        QWebPage.__init__(self)  
        self.loadFinished.connect(self._loadFinished)  
        self.mainFrame().load(QUrl(url))  
        self.app.exec_()
    def _loadFinished(self, result):  
        self.frame = self.mainFrame()  
        self.app.quit() 

r = Render(seeking_alpha_url)
result = r.frame.toHtml()
soup = BeautifulSoup(result, 'html.parser')

ul_elements = soup.find("ul", {"id": "symbol-page-latest"})
li_elements = ul_elements.findAll("li", {"class": "symbol_item"})
news_dict_lst = []
for potential_li_element in li_elements:
    title_parent = potential_li_element.find("div", {"class": "symbol_article"})
    date_element_parent = potential_li_element.find("div", {"class": "date_on_by"})
    article_title = title_parent.find("a").text
    try:
        article_href = title_parent.find("a").attrs['href']
    except:
        continue
    if article_href[0] == "/":
        article_href =  "https://seekingalpha.com" + article_href
    news_dictionary = {}
    date_element_spans = date_element_parent.findAll("span")
    try:
        date_element = date_element_spans[1].text
    except:
        continue
    news_dictionary['date'] = date_element
    news_dictionary['article-title'] = article_title
    news_dictionary['article-uri'] = article_href
    news_dictionary_json = json.dumps(news_dictionary)
    news_dict_lst.append(news_dictionary_json)
print(json.dumps(news_dict_lst))
sys.stdout.flush()