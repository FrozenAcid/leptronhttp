import dryscrape
from bs4 import BeautifulSoup
import sys
from PyQt5.QtGui import *  
from PyQt5.QtCore import *  
from PyQt5.QtWebKit import *
from PyQt5.QtWebKitWidgets import QWebPage
from PyQt5.QtWidgets import QApplication
import os
import lxml
import json
import requests
import re

os.environ['QT_QPA_PLATFORM']='offscreen'

ticker = sys.argv[1]
iex_url = "https://api.iextrading.com/1.0/stock/" + ticker + "/quote"   
iex_r = requests.get(iex_url).text
iex_dict = json.loads(iex_r)
iex_name = iex_dict['companyName']

iex_name = re.sub('\Inc.$', '', iex_name)
iex_name = re.sub('\Corporation$', '', iex_name)
iex_symbol = iex_dict['symbol']
iex_exchange = iex_dict['primaryExchange']

if iex_exchange == "Nasdaq Global Select":
    mot_exchange = "nasdaq"
    mot_bool = True
if iex_exchange == "New York Stock Exchange":
    mot_exchange = "nyse"
    mot_bool = True
else:
    mot_bool = False

if mot_bool == False:
    print("no stock")