var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/leptron";
var express = require("express");
var app = express();
var cors = require('cors');
var fs = require('fs')
var uniqid = require('uniqid');	
var async = require('async')
var morgan = require('morgan');
app.use(morgan())

app.use(cors())

app.get('/', function(req, res) {
    return res.json({serverStatus: true})
})

app.get('/stock_news', function(req, res) {
	var ticker = req.query.ticker

	const spawn = require("child_process").spawn;
	const pythonProcess = spawn('python',["scraper_lib/scraper_script_seeking_alpha.py", ticker]);
	pythonProcess.stdout.on('data', (data) => {
		var string = data.toString()
		var parsed = JSON.parse(string)
		for(var i = 0; i != parsed.length; i++) {
			console.log(i)
		}
		return res.send(string)
	});
})


async.parallel([
	function(callback) {
		var minutes = 1, the_interval = minutes * 60 * 1000;
		setInterval(function() {
			console.log("starting scrape")
			console.log("getting stock list")
			MongoClient.connect(url, function(err, db) {
				console.log("connected to db")
				var dbo = db.db("leptronDataBase")
				console.log('finding stock lists')
				dbo.collection("newsStockScrape").findOne({id: "stock_scrape_lst"}, function(err, dbRes) {
					if(err) throw err;
					const list = dbRes.stocks
					for(var i = 0; i != list.length; i++) {
						const stock = list[i]
						const spawn = require("child_process").spawn;
						dbo.collection("newsStockScrape").findOne({id: "counter", stock: "aapl"}, function(err, dbResCounter) {
							if(err) throw err;
							var current_count = dbResCounter.count
							const pythonProcess = spawn('python',["scraper_lib/scraper_script_seeking_alpha.py", stock]);
							pythonProcess.stdout.on('data', (data) => {
								var string = data.toString()
								var parsed = JSON.parse(string)
								var seeking_alpha_links = []
								for(var i = 0; i != parsed.length; i++) {
									const unparsed_news = parsed[i]
									const parsed_news_json = JSON.parse(unparsed_news)
									seeking_alpha_links.push(parsed_news_json)
								}
								db_push_dict = {}
								db_push_dict['counter'] = current_count
								db_push_dict['ticker'] = stock
								db_push_dict['seeking_alpha_news'] = seeking_alpha_links
								console.log(db_push_dict)
							})
						})
					}
				})
			})
		}, the_interval)
	},
	function(callback) {
		app.listen(3030)
	}
])
