var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/leptron";
var express = require("express");
var app = express();
var cors = require('cors');
var fs = require('fs')
var uniqid = require('uniqid');	
var morgan = require('morgan');
const puppeteer = require('puppeteer');

app.use(morgan())

app.use(cors())

app.get('/', function(req, res) {
    return res.json({serverStatus: true})
})

var privateKEY = fs.readFileSync('./keys/private.key');
var publicKEY = fs.readFileSync('./keys/public.key')
const jwt  = require('jsonwebtoken');

const i = "Leptron"
const s = "leptronUser"
const a = "leptronDev"

var signingOptions = {
	issuer: i,
	subject: s,
	audience: a,
	expiresIn: "12h",
	algorithm: "RS256"
}

var session_json = {}
app.get('/getKey', function(req, res) {
    return res.send(privateKEY)
})
app.get('/auth_session', function(req, res) {
	session_id = req.query.session_id
	if(session_json[session_id] != null) {
		console.log("has session")
		return res.json({session: true})
	}
	else {
		return res.json({session: false})
	}
})
app.get('/login', function(req, res) {
	payload = req.query.payload
	var unenCrypt = jwt.verify(payload, publicKEY, {algorithms: "RS256"})
	console.log(unenCrypt)
	MongoClient.connect(url, function(err, db) {
		if(err) throw err;
		console.log('connected')
		var dbo = db.db("leptronDataBase")
		dbo.collection("userLoginData").findOne({uname: unenCrypt['uname']}, function(err, dbRes) {
			if(err) throw err;
			console.log("in collectino" + dbRes)
			if(dbRes == null) {
				console.log("NO DB RES:(")
				db.close()
				return res.json({user_logged: false})
			}
			else {
				//comparing passwords
				if(unenCrypt['psswd'] == dbRes['psswd']) {
					const uniqIDGEN = uniqid();
					session_json[uniqIDGEN] = unenCrypt['uname']
					db.close()
					return res.json({user_logged: true, session_id: uniqIDGEN})
				}
				else {
					db.close()
					return res.json({user_logged: false})
				}
			}
		})
	})
})
app.get('/signout', function(req, res) {
	session_id = req.query.session_id
	delete session_json[session_id]
	return res.json({logged_out: true})
})
app.get('/create_user', function(req, res) {

    payload = req.query.payload
	var unenCrypt = jwt.verify(payload, publicKEY, {algorithm: "RS256"})
	
	MongoClient.connect(url, function(err, db) {
		if(err) throw err;

		var dbo = db.db("leptronDataBase")
		dbo.collection("userLoginData").findOne({uname: unenCrypt['uname']}, function(err, dbRes) {
			if(err) throw err;
			if(dbRes == null) {
				dbo.collection("userLoginData").insertOne(unenCrypt, function(err, dbRes) {
					if (err) throw err 
					db.close()
					const uniqIDGEN = uniqid();
					session_json[uniqIDGEN] = unenCrypt['uname']
					return res.json({userRegisterTry: true, session_id: uniqIDGEN})
				})
			}
			else {
				db.close()
				return res.json({userRegisterTry: false})
			}
		})
	})
})

app.listen(8080)
