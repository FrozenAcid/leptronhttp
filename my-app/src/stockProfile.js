import React, { Component } from 'react';
import "./app.min.css"  
import "./stockComponents/inlineCss/news.css"
import "./stockComponents/inlineCss/stock.css"
import "./stockComponents/inlineCss/profile.css"
import "./stockComponents/inlineCss/inline.css"
import axios from 'axios';
import {KJUR} from 'jsrsasign';
import {Dispatcher} from 'flux';
import {Line} from "react-chartjs-2";
import Finance from "./stockComponents/financeTable.js"
import StockChart from "./stockComponents/stockChart.js"
import ProfileComponent from "./stockComponents/profileComponent.js"
import ResultsStockComponent from "./stockComponents/results.js"
import NewsComponent from "./stockComponents/newsComponent.js"

class StockProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ticker: this.props.stock,
            companyName: null,
            price: null,
            volume: null,
            prevClose: null,
            change: null,
            peRat: null,
            current_class: "a",
            aStyle: null,
            bStyle: {display: "none"},
            cStyle: {display: "none"}
        }
    }
    componentDidMount() {
        const url = "https://api.iextrading.com/1.0/stock/" + this.state.ticker + "/batch?types=quote"
        axios.get(url)
            .then(res => {
                const quote = res.data.quote;
                this.setState({companyName: quote.companyName, price: quote.latestPrice, volume: quote.latestVolume,
                prevClose: quote.previousClose, change: quote.change, peRat: quote.peRatio})
            })
    }

    render() {
        return (
            <div className="container-fluid pl-0 pr-0" style={{color: "black"}}>
                <div className="row">
                    <div className="col-12">
                        <div className="page-title-box">
                            
                        </div>
                    </div>
                </div>
                <div className="card shadow-lg mt-3">
                    <div className="card-header">
                        <h4 className="page-title ml-4">{this.state.ticker} ({this.state.companyName}) - ${this.state.price}</h4>
                    </div>
                    <div className="card-body ">
                        <div className="row mb-4 pb-4" style={{margin: "0 auto"}}>
                            <div className="col-sm-2"></div>
                            <div className="col-sm-8 mt-4">
                                <div className="chartjs-chart" style={{height: "300px"}}>
                                    <StockChart ticker={this.state.ticker} />
                                </div>
                            </div>
                        </div>
                        <div className="row container mt-4 pt-4" style={{margin: "0 auto"}}>
                            <div className="col-xs-12 col-md-12 col-lg-6 col-sm-12 ml">
                                <ProfileComponent stock={this.state.ticker} />
                            </div>
                            <div className="col-xs-12 col-md-12 col-lg-6 col-sm-12 ml">
                                <ResultsStockComponent stock={this.state.ticker} />
                            </div>
                        </div>
                        <div className="row container mt-3" style={{margin: "0 auto"}}>
                            <div className="pt-2 col-xs-12">
                                    <NewsComponent stock={this.state.ticker} />
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default StockProfile;