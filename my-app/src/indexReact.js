import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "./app.min.css"
import axios from 'axios';
import {KJUR} from 'jsrsasign';
import {Dispatcher} from 'flux';

class Stock extends Component {
    constructor(props) {
        super(props);
        this.state = {
            price: 0,
            ticker: props.tck,
            fullName: "(name to be inserted)",
            changePercent: 0,
            textColorState: "text-success mr-2"
        }
        this.onclickHandler = this.onclickHandler.bind(this)
        this.tick = this.tick.bind(this)
    }
    componentDidMount() {
        const url = "https://api.iextrading.com/1.0/stock/" + this.state.ticker + "/batch?types=quote"
        axios.get(url)
            .then(res => {
                const quote = res.data.quote;
                const changePercent = quote.changePercent
                this.setState({fullName: quote.companyName})
                this.setState({price: quote.latestPrice})
                this.setState({changePercent: quote.changePercent})
                if(changePercent > 0) {
                    this.setState({textColorState: "text-success mr-2"})
                }
                else {
                    this.setState({textColorState: "text-danger mr-2"})
                }
            })
        this.interval = setInterval(() => this.tick(), 50000);
    }
    tick() {
        const url = "https://api.iextrading.com/1.0/stock/" + this.state.ticker + "/batch?types=quote"
        axios.get(url)
            .then(res => {
                const quote = res.data.quote;
                const changePercent = quote.changePercent
                this.setState({fullName: quote.companyName})
                this.setState({price: quote.latestPrice})
                this.setState({changePercent: quote.changePercent})
                if(changePercent > 0) {
                    this.setState({textColorState: "text-success mr-2"})
                }
                else {
                    this.setState({textColorState: "text-danger mr-2"})
                }
            })
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }
    onclickHandler() {
        window.location.replace("/stock/" + this.state.ticker) //TODO make sure to add reload functionality
    }
    render() {
        return (
            <div className="col-lg-4" >
                <div className="card widget-flat" onClick={this.onclickHandler}>
                    <div className="card-body">
                        <h5 className="text-muted font-weight-normal mt-0">{this.state.ticker} - {this.state.fullName}</h5>
                        <h3 className="mt-3 mb-3">{this.state.price}</h3>
                        <p className="mb-0 text-muted">
                            <span className={this.state.textColorState}>
                                {this.state.changePercent}
                            </span>
                            <span className="text-nowrap">Since Last Trading Day</span>
                        </p>
                    </div>
                </div>
            </div>
        )
    }
}
const dispatcher = new Dispatcher()
class FailNotification extends Component {
    closeHandler() {
        console.log("closing")
        dispatcher.dispatch({type: 'close'})
    }
    render() {
        return (
            <div className="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                <button type="button" className="close" onClick={this.closeHandler} data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>Error - </strong>"Login Try Unsuccessful"
            </div>
        )
    }
}

class Modal extends Component {
    constructor() {
        super()
        this.state = {
            key: null,
            serverUrl: "http://localhost:8080",
            notification: false
        }
        this.componentWillMount = this.componentWillMount.bind(this)
        this.submitHandler = this.submitHandler.bind(this)
    }
    componentWillMount() {
        axios.get(this.state.serverUrl + "/getKey")
            .then(res => {
                var key = res.data
                this.setState({key: key})
            })
    }
    componentDidMount() {
        dispatcher.register(dispatch => {
            if(dispatch.type === 'close') {
                this.setState({notification: false})
            }
        })
    }
    submitHandler() {
        var uname_val = document.getElementById("uname").value
        var psswd_val = document.getElementById("psswd").value
        if(uname_val != "" && psswd_val != "") {
            var payload = {uname: uname_val, psswd: psswd_val}
            var sJWS = KJUR.jws.JWS.sign("RS256", JSON.stringify({alg: "RS256"}), JSON.stringify(payload), this.state.key)
            axios.get(this.state.serverUrl + `/login?payload=${sJWS}`)
                .then(res => {
                    if(res.data.user_logged == false) {
                        this.setState({notification: true})
                    }
                    if(res.data.user_logged == true) {
                        sessionStorage.setItem("session_id", res.data.session_id)
                        dispatcher.dispatch({type: 'sessionIDSet'})
                    }
                })
        }
    }
    render() {
        var notification;
        if(this.state.notification) {
            console.log('oteematic ivy!')
            notification = <FailNotification />
        }
        return (
            <div id="loginModal" className="modal fade" tabIndex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
                {notification}
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title" id="loginModalLabel">Leptron - Login</h4>
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <div className="modal-body">
                            <form >
                                <div className="form-group">
                                    <label style={{fontFamily: "Nunito"}} for="uname">Username</label>
                                    <input className="form-control" type="username" id="uname" required placeholder="Enter Username"></input>
                                </div>
                                <div className="form-group">
                                    <label style={{fontFamily: "Nunito"}} for="psswd">Password</label>
                                    <input className="form-control" type="password" id="psswd" required placeholder="Enter Password"></input>
                                </div>
                            </form>
                            <button className="btn btn-primary" onClick={this.submitHandler}>Submit</button>
                        </div>
                        <a href="/signup"> OR you can signup here</a>
                    </div>
                </div>
            </div>
        )
    }
}

class Index extends Component {
    constructor() {
        super();
        this.state = {
            stock_lst: ["AAPL","GOOGL"],
            loggedIn: false,
            loginStateText: "Login",
            loginTitleStateText: "Stocks to Get You Started",
            session_id: sessionStorage.getItem("session_id"),
            server_ip: "http://localhost:8080"
        };
        this.componentWillMount = this.componentWillMount.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)
        this.loginHandler = this.loginHandler.bind(this)
    }
    componentWillMount() {
        if(this.state.session_id == null) {
            this.setState({loggedIn: false, loginStateText: "Login"})
        }
        if(this.state.session_id != null) {
            var auth_session_id_uri = this.state.server_ip + "/auth_session?session_id=" + this.state.session_id
            axios.get(auth_session_id_uri)
                .then(res => {
                    if(res.data.session == false) {
                        sessionStorage.setItem("session_id",null)
                    }
                    else {
                        this.setState({loggedIn: true, loginStateText: "Signout"})
                    }
                })
        }
    }
    componentDidMount() {
        dispatcher.register(dispatch => {
            if(dispatch.type === 'sessionIDSet') {
                window.location.reload()
            }
        })
    }
    menuClick() {
        document.body.classList.add('sidebar-enable')
    }
    loginHandler() {
        if(this.state.loggedIn == false) {
            document.getElementById("login-check").click()
        }
        if(this.state.loggedIn == true) {
            var req_url = this.state.server_ip + "/signout?session_id=" + this.state.session_id
            axios.get(req_url)
                .then(res => {
                    if(res.data.logged_out) {
                        window.location.reload()
                    }
                })
        }
    }
    searchSubmitHandler(event) {
        event.preventDefault()
        var search_val = document.getElementById("searchForm").value
        window.location.replace("/stock/" + search_val)
    }
    render() {
        const listItems = this.state.stock_lst.map((stock) =>
            <Stock key={stock} tck={stock} />
        );
        return (
            <div className="wrapper">

                <div className="left-side-menu active">
                    <div className="slimscroll-menu">
                        <a href="/" className="logo text-center mb-4">
                            <span className="logo-lg" style={{color: 'white', fontSize: "40px"}}>
                                Leptron
                            </span>
                            <span className="logo-sm" style={{color: 'white', fontSize: "40px"}}>
                                LEP
                            </span>
                        </a>
                        <ul className="metismenu side-nav">
                            <li className="side-nav-title side-nav-item">Navigation</li>
                            <li className="side-nav-item active">
                                <a href="/" className="side-nav-link">
                                    <i className="material-icons">show_chart</i>
                                    <span className="float-right">
                                        <i className="material-icons">keyboard_arrow_right</i>
                                    </span>
                                    <span> Stocks </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="content-page">
                    <div className="content">
                        <ul className="list-unstyled topbar-right-menu float-right mb-0">
                            <li style={{paddingTop: "16px"}}>
                                <button style={{display: "none"}} id="login-check" data-toggle="modal" data-target="#loginModal"></button>
                                <button className="btn btn-primary" onClick={this.loginHandler}>{this.state.loginStateText}</button>
                            </li>
                        </ul>
                        <div className="navbar-custom">
                            <div className="app-search ml-4">
                                <form onSubmit={this.searchSubmitHandler}>
                                    <div className="input-group">
                                        <input type="text" className="form-control" id="searchForm" placeholder="Search . . ." />
                                        <span className="mdi" style={{paddingTop: ".35rem"}}>
                                            <i className="material-icons">search</i>
                                        </span>
                                        <div className="input-group-append">
                                            <button className="btn btn-primary" type="submit">Search</button>
                                        </div>

                                    </div>
                                </form>
                            </div>

                            <button onClick={this.menuClick} className="button-menu-mobile open-left disable-btn">
                                <i className="material-icons">menu</i>
                            </button>

                        </div>

                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-12">
                                    <div className="page-title-box">
                                        <h2 className="page-title">{this.state.loginTitleStateText}</h2>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xl-12">
                                    <div className="row mt-3">
                                        {listItems}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Modal />
                    </div>
                </div>

            </div>
        )
    }
}

export default Index;