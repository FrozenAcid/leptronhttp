import React, { Component } from 'react';
import "./app.min.css"
import axios from 'axios';
import {KJUR} from 'jsrsasign';

class FailNotification extends Component {
    render() {
        return (
            <div className="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show" role="alert">
                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>Error - </strong>"Signup Try Unsuccessful (Page will Automatically Refresh)"
            </div>
        )
    }
}

class Signup extends Component {
    constructor(props) {
        super(props)
        this.state = {
            serverUrl: "http://localhost:8080",
            key: null,
            notificationFailStatus: null
        }
        this.submitHandler = this.submitHandler.bind(this)
        this.render = this.render.bind(this)
    }
    componentWillMount() {
        axios.get(this.state.serverUrl + "/getKey")
            .then(res => {
                var key = res.data
                this.setState({key: key})
            })
    }
    submitHandler() {
        var uname_val = document.getElementById("uname").value
        var psswd_val = document.getElementById("psswd").value
        if(psswd_val != "" && uname_val != "") {
            console.log("kept")
            var payload_obj = {}
            payload_obj['uname'] = document.getElementById("uname").value
            payload_obj['psswd'] = document.getElementById("psswd").value

            var sJWS = KJUR.jws.JWS.sign("RS256", JSON.stringify({alg: "RS256"}), JSON.stringify(payload_obj), this.state.key)
            axios.get(this.state.serverUrl + "/create_user?payload=" + sJWS)
                .then(res => {
                    const resData = res.data
                    if(resData.userRegisterTry == true) {
                        sessionStorage.setItem('session_id', resData.session_id)
                        window.location.replace("/")
                    }
                    else {
                        this.setState({notificationFailStatus: true})
                    }
                })
        }
    }
    render() {
        var notification;
        if(this.state.notificationFailStatus) {
            console.log('notification fail')
            notification = <FailNotification />
            setTimeout(() => {
                window.location.reload();
            },1500)
        }
        return (
            <div>
                <script
                src="https://code.jquery.com/jquery-3.3.1.min.js"
                integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
                crossOrigin="anonymous"></script>   
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
                <script src="https://cdnjs.cloudflare.com/ajax/libs/cryptico/0.0.1343522940/cryptico.js"></script>
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
                <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"></link>
                <script src="js/app.min.js"></script>
                <link rel="stylesheet" href="http://cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css" />
                <script language="JavaScript" type="text/javascript"
                src="http://kjur.github.io/jsrsasign/jsrsasign-latest-all-min.js"></script>
                <script src="js/app.min.js"></script>
                <div className="account-pages mt-5 mb-5">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-lg-5">
                            {notification}
                                <div className="card">
                                    <div className="card-header pt-4 pb-4 text-center bg-primary">
                                        <a href="/" style={{color: "white"}}><h3>Leptron - Signup</h3></a>
                                    </div>
                                    <div className="card-body p-4">
                                        <div className="text-center w-75 m-auto">
                                            <h4 className="text-dark-50 text-center mt-0 font-weight-bold">Free Sign Up</h4>
                                            <p className="text-muted mb-4">Dont Have an Account? Dont worry, signup its free and reap all the benifets of being a member</p>
                                        </div>
                                        <form action="#">
                                            <div className="form-group">
                                                <label htmlFor="uname">User Name</label>
                                                <input className="form-control" type="text" id="uname" placeholder="Enter Your Username" />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="psswd">Password</label>
                                                <input className="form-control" type="password" id="psswd" placeholder="Enter Your Password" />
                                            </div>
                                        </form>
                                        <button className="btn btn-primary" onClick={this.submitHandler}>Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Signup;