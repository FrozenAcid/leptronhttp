import axios from 'axios';
import React, { Component } from 'react';

class ResultsStockComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ticker: this.props.stock
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount() {
        const url = "https://api.iextrading.com/1.0/stock/" + this.state.ticker + "/quote"
        axios.get(url) 
            .then(res => {
                this.setState({open_price: res.data.open, volume: res.data.latestVolume, change: res.data.change, avgTotalVolume: res.data.avgTotalVolume, peRatio: res.data.peRatio, marketCap: res.data.marketCap})
            })
            .then(() => {
                const next_url = "https://api.iextrading.com/1.0/stock/" + this.state.ticker + "/stats"
                axios.get(next_url)
                    .then(next_res => {
                        this.setState({beta: next_res.data.beta, week52high: next_res.data.week52high, week52low: next_res.data.week52low, week52change: next_res.data.week52change, 
                            dividendRate: next_res.data.dividendRate, dividendYield: next_res.data.dividendYield, float: next_res.data.float})
                    })
            })
    }
    render() {
        return (
            <div className="text-center">
                <div className="small-text border-bottom">
                    <h3 className="pb-2">Stock Preformance</h3>
                </div>
                <div className="row pt-2" style={{overflowX: "scroll"}}>
                    <div className="col-4">
                        <p className="descriptor-title pb-4 mb-3">Open Price</p>
                        <h5 className="">{this.state.open_price}</h5>
                    </div>
                    <div className="col-4">
                        <p className="descriptor-title pb-4 mb-3">Volume</p>
                        <h5 className="">{this.state.volume}</h5>
                    </div>
                    <div className="col-4">
                        <p className="descriptor-title pb-4 mb-3">Change (In USD)</p>
                        <h5 className="">{this.state.change}</h5>
                    </div>
                </div>
                <div className="row pt-3" style={{overflowX: "scroll"}}>
                    <div className="col-4">
                        <p className="descriptor-title pb-4 mb-3">Avg Volume</p>
                        <h5 className="">{this.state.avgTotalVolume}</h5>
                    </div>
                    <div className="col-4">
                        <p className="descriptor-title pb-4 mb-3">Pe Ratio</p>
                        <h5 className="">{this.state.peRatio}</h5>
                    </div>
                    <div className="col-4">
                        <p className="descriptor-title pb-4 mb-3">Market Cap</p>
                        <h5 className="">{this.state.marketCap}</h5>
                    </div>
                </div>
                <div className="row pt-3" style={{overflowX: "scroll"}}>
                    <div className="col-4">
                        <p className="descriptor-title pb-4 mb-3">Beta</p>
                        <h5 className="">{this.state.beta}</h5>
                    </div>
                    <div className="col-4">
                        <p className="descriptor-title pb-4 mb-3">52 Week High</p>
                        <h5 className="">{this.state.week52high}</h5>
                    </div>
                    <div className="col-4">
                        <p className="descriptor-title pb-4 mb-3">52 Week Low</p>
                        <h5 className="">{this.state.week52low}</h5>
                    </div>
                </div>
                <div className="row pt-3" style={{overflowX: "scroll"}}>
                    <div className="col-4">
                        <p className="descriptor-title pb-4 mb-3">52 Week Change</p>
                        <h5 className="">{this.state.week52change}</h5>
                    </div>
                    <div className="col-4">
                        <p className="descriptor-title pb-4 mb-3">Dividend Rate</p>
                        <h5 className="">{this.state.dividendRate}</h5>
                    </div>
                    <div className="col-4">
                        <p className="descriptor-title pb-4 mb-3">Dividend Yield</p>
                        <h5 className="">{this.state.dividendYield}</h5>
                    </div>
                </div>
                <div className="row pt-3">
                    <div className="col-4">
                        <p className="descriptor-title pb-4 mb-3">Float</p>
                        <h5 className="">{this.state.float}</h5>
                    </div>
                </div>
            </div>
        )
    }
}
export default ResultsStockComponent;