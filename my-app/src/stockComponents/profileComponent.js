import React, { Component } from 'react';
import axios from 'axios';
import {KJUR} from 'jsrsasign';
import {Dispatcher} from 'flux';
import {Line} from "react-chartjs-2";
import { stat } from 'fs';

class ProfileComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ticker: this.props.stock,
            exchange: "...",
            industry: "...",
            sector: "...",
            employee_num: 0
        }
    }
    componentDidMount() {
        const url = "https://api.iextrading.com/1.0/stock/" + this.state.ticker + "/company"
        const next_url = "https://api.iextrading.com/1.0/stock/" + this.state.ticker + "/stats"
        axios.get(url)
            .then(res => {
                this.setState({description: res.data.description, exchange: res.data.exchange, industry: res.data.industry, sector: res.data.sector, website: res.data.website})
            })
            .then(() => {
                axios.get(next_url)
                    .then(statRes => {
                        const revenue = statRes.data.revenue
                        const revPerEmp = statRes.data.revenuePerEmployee
                        this.setState({employee_num: parseInt(revenue / revPerEmp), float: statRes.data.float, sharesOutstanding: statRes.data.sharesOutstanding})
                    })
            })
    }
    render() {
        return (
            <div className="text-center" style={{margin: "0 auto"}}>
                <div className="small-text border-bottom">
                    <h3 className=" pb-2">Profile</h3>
                    <p >{this.state.description}</p>
                </div>
                <div className="" style={{margin: "0 auto"}}>
                    <div className="row pt-3">
                        <div className="col-md-1 col-sm-0"></div>
                        <div className="col-md-5 pl-md-4 col-sm-12 col-xs-12">
                            <p className="descriptor-title pb-4">Stock Exchange</p>
                            <h5 className="pl-">{this.state.exchange}</h5>
                        </div>
                        <div className="col-md-5 col-sm-12 col-xs-12">
                            <p className="descriptor-title ">Industry</p>
                            <h5 className="">{this.state.industry}</h5>
                        </div>
                    </div>
                    <div className="row pt-2" style={{margin: "0 auto"}}>
                        <div className="col-md-1 col-sm-0"></div>
                        <div className="col-md-5 col-sm-12 col-xs-12">
                            <p className="descriptor-title ">Website</p>
                            <h5 className=""><a target="_blank" href={this.state.website}>{this.state.website}</a></h5>
                        </div>
                        <div className="col-md-5 col-sm-12 col-xs-12 custom-css-center-md">
                            <p className="descriptor-title">Sector</p>
                            <h5 className="">{this.state.sector}</h5>
                        </div>
                    </div>
                    <div className="row pt-2 " style={{margin: "0 auto"}}>
                        <div className="col-md-1 col-sm-0"></div>
                        <div className="col-md-5 col-sm-12 col-xs-12">
                            <p className="descriptor-title pb-4">Number Of Employees</p>
                            <h5 className="">{this.state.employee_num}</h5>
                        </div>
                        <div className="col-md-5 col-sm-12 col-xs-12">
                            <p className="descriptor-title">Float</p>
                            <h5 className="">{this.state.float}</h5>
                        </div>
                    </div>
                    <div className="row pt-2" style={{margin: "0 auto"}}>
                        <div className="col-md-1 col-sm-0"></div>
                        <div className="col-md-5 col-sm-12 col-xs-12">
                            <p className="descriptor-title pb-4">Shares Outstanding</p>
                            <h5 className="">{this.state.sharesOutstanding}</h5>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ProfileComponent;