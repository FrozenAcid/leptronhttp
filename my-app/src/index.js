import React from 'react';
import ReactDOM from 'react-dom';
import Index from './indexReact';
import Signup from './signup';
import Stock from './stock';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Route, Link } from "react-router-dom";

ReactDOM.render(

    <BrowserRouter>
        <div>
            <Route exact path="/" component={Index} />
            <Route exact path="/signup" component={Signup} />
            <Route path="/stock/:name" component={Stock} />
        </div>
    </BrowserRouter>
    
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
