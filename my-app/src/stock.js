import React, { Component } from 'react';
import "./app.min.css"
import axios from 'axios';
import {KJUR} from 'jsrsasign';
import {Dispatcher} from 'flux';
import {Line} from "react-chartjs-2";
import StockProfile from "./stockProfile.js"

class Popup extends Component {
    componentDidMount() {
        document.body.classList.add('modal-open')
    }
    render() {
        return (
            <div id="fill-primary-modal" className="modal fade show" style={{display: "block"}} tabindex="-1" role="dialog"  data-backdrop="static" aria-labelledby="fill-primary-modalLabel">
                <div className="modal-dialog">
                    <div className="modal-content modal-filled bg-primary">
                        <div className="modal-header">
                            <h4 className="modal-title" id="fill-primary-modalLabel">ALERT</h4>
                        </div>
                        <div className="modal-body">
                            <p> This Page is restricted because you are not logged into our site</p>
							<p>Either log onto our website on the <a style={{color: "white", fontWeight: "900"}} href="/"> Homepage </a></p>
							<p>Or register on the signup page <a style={{color: "white", fontWeight: "900"}} href="/signup">Here</a></p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



class Stock extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ticker: this.props.match.params.name,
            loggedIn: false,
            session_id: sessionStorage.getItem("session_id"),
            server_ip: "http://localhost:8080",
            navbarStyle: {display: "block"},
            componentState: null
        }
        this.componentWillMount = this.componentWillMount.bind(this)
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this)
    }
    componentWillMount() {
        console.log('pre mount')
    }
    componentDidMount() {
        if(this.state.session_id == null) {
            console.log('no session_id')
            this.setState({loggedIn: false, loginStateText: "Login"})
            this.setState({componentState: <Popup />, navbarStyle: {display: "none"}})
        }
        if(this.state.session_id != null) {
            var auth_session_id_uri = this.state.server_ip + "/auth_session?session_id=" + this.state.session_id
            axios.get(auth_session_id_uri)
                .then(res => {
                    console.log(res)
                    if(res.data.session == false) {
                        sessionStorage.setItem("session_id",null)
                        this.setState({componentState: <Popup />, navbarStyle: {display: "none"}})
                    }
                    if(res.data.session == true) {
                        this.setState({componentState: <StockProfile stock={this.state.ticker} />})
                        this.setState({loggedIn: true, loginStateText: "Signout"})
                        this.updateWindowDimensions();
                        window.addEventListener('resize', this.updateWindowDimensions);
                    }
                })
        }
    }
    searchSubmitHandler(event) {
        event.preventDefault()
        var search_val = document.getElementById("searchForm").value
        window.location.replace("/stock/" + search_val)
    }
    menuClick() {
        document.body.classList.add('sidebar-enable')
    }
    menuOff() {
        if(document.body.classList[0] == "sidebar-enable") {
            document.body.classList.remove('sidebar-enable')
        }
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
      }
      
      updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
      }
    render() {
        if( this.state.width <= 1024 && this.state.width > 475) {
            document.body.classList.add("enlarged")
        }
        if(!(this.state.width <= 1024 && this.state.width > 475) ) {
            document.body.classList.remove("enlarged")
        }
        return (
            <div className="wrapper">

                <div className="left-side-menu active">
                    <div className="slimscroll-menu">
                        <a href="/" className="logo text-center mb-4">
                            <span className="logo-lg" style={{color: 'white', fontSize: "40px"}}>
                                Leptron
                            </span>
                            <span className="logo-sm" style={{color: 'white', fontSize: "40px"}}>
                                LEP
                            </span>
                        </a>
                        <ul className="metismenu side-nav">
                            <li className="side-nav-title side-nav-item">Navigation</li>
                            <li className="side-nav-item active">
                                <a href="/" className="side-nav-link">
                                    <i className="material-icons">show_chart</i>
                                    <span className="float-right">
                                        <i className="material-icons">keyboard_arrow_right</i>
                                    </span>
                                    <span> Stocks </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="content-page">
                    <div className="content">
                        <div className="navbar-custom" style={this.state.navbarStyle}>
                            <div className="app-search ml-4">
                                <form onSubmit={this.searchSubmitHandler}>
                                    <div className="input-group">
                                        <input type="text" className="form-control" id="searchForm" placeholder="Search . . ." />
                                        <span className="mdi" style={{paddingTop: ".35rem"}}>
                                            <i className="material-icons">search</i>
                                        </span>
                                        <div className="input-group-append">
                                            <button className="btn btn-primary" type="submit">Search</button>
                                        </div>

                                    </div>
                                </form>
                            </div>

                            <button onClick={this.menuClick} className="button-menu-mobile open-left disable-btn">
                                <i className="material-icons">menu</i>
                            </button>

                        </div>
                        <div onClick={this.menuOff}>
                            {this.state.componentState}
                        </div>

                    </div>
                </div>

            </div>
        )
    }
}
export default Stock;