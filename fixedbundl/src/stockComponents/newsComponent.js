import React, { Component } from 'react';
import axios from 'axios';
import {KJUR} from 'jsrsasign';
import {Dispatcher} from 'flux';
import {Line} from "react-chartjs-2";
import Headline from "./newsComponents/headline.js"
class NewsComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ticker: this.props.stock,
            news_lst: []
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount() {
        const url = "https://api.iextrading.com/1.0/stock/" + this.state.ticker + "/news"
        axios.get(url)
            .then(res => {
                for(var i = 0; i != res.data.length; i++) {
                    const news_headline = res.data[i]
                    const datetime_string = news_headline.datetime + " | " + news_headline.source
                    const title_string = news_headline.headline
                    const description_string = news_headline.summary
                    const dataPackage = {
                        dateString: datetime_string,
                        titleString: title_string,
                        descriptionString: description_string,
                        url: news_headline.url
                    }
                    console.log(dataPackage)
                    this.setState({news_lst: [...this.state.news_lst, <Headline data={dataPackage} />]})
                }
            })
    }
    render() {
        return (
            <div>
                <div className="border-bottom">
                    <h3 className="pb-3">News</h3>
                </div>
                <div className="container" style={{overflowX: "scroll"}}>
                    {this.state.news_lst}
                </div>
            </div>
        )
    }
}
export default NewsComponent;