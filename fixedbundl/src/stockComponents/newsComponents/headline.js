import React, { Component } from 'react';
import axios from 'axios';
import {KJUR} from 'jsrsasign';
import {Dispatcher} from 'flux';
import {Line} from "react-chartjs-2";

class Headline extends Component {
    constructor(props) {
        super(props)
        this.state = {
            headlineJSON: this.props.data
        }
    }
    render() {
        return(
            <div className="rounded md-py1 mb1"><div className="col-12 rounded" style={{height: 'auto', verticalAlign: 'top'}}><div className="md-flex"><div className="md-flex flex-1 border-box md-pr3 py1"><div className="md-flex flex-1 block"><a className="md-flex" href={this.state.headlineJSON.url} target="_blank" rel="noopener noreferrer" style={{width: '100%', height: '100%', minHeight: '144px'}}><div className="md-flex flex-1 block bg-light-silver" style={{backgroundImage: 'url("https://api.iextrading.com/1.0/stock/amd/news-image/7481615283420655")', backgroundPosition: '50% center', backgroundSize: 'cover', backgroundRepeat: 'no-repeat', backgroundOrigin: 'border-box', width: '100%', height: '100%', borderRadius: '0.1875rem', minHeight: '144px'}} /></a></div></div><div className="md-flex flex-2 border-box"><div className="md-flex flex-2 block flex-column"><div className="relative" style={{color: 'rgb(68, 68, 68)'}}>
            <p className="h6 mt1 regular mb0 flex-column">{this.state.headlineJSON.dateString}</p></div>
            <a className="md-flex" href={this.state.headlineJSON.url} target="_blank" rel="noopener noreferrer" style={{color: 'rgb(34, 34, 34)'}}><div className="py1 relative flex-column">
            <p className="medium-text mb0 bold">{this.state.headlineJSON.titleString}</p>
            <p className="small-text mt1 mb1" style={{lineHeight: '1.5'}}>{this.state.headlineJSON.descriptionString}</p>
            </div></a><div className="mb1 inline-block mxn1 gray"><div className="px1 inline-block"></div></div></div></div></div></div></div>
        )
    }
}
export default Headline;