
import React, { Component } from 'react';
import axios from 'axios';
import {KJUR} from 'jsrsasign';
import {Dispatcher} from 'flux';
import {Line} from "react-chartjs-2";

class PeerComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ticker: this.props.ticker,
            fullName: null,
            price: null
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount() {
        var url = "https://api.iextrading.com/1.0/stock/" + this.state.ticker + "/quote"
        axios.get(url)
            .then(res => {
                const data = res.data 
                this.setState({fullName: data.companyName, price: data.latestPrice})
            })
    }   
    render() {
        return (
            <div className="card-body">          
                <div className="float-left">
                    <h4>{this.state.ticker} - {this.state.fullName}   (${this.state.price})</h4>
                </div>
            </div>
        )
    }
}
class Peers extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ticker: this.props.ticker,
            peers: []
        }
        this.componentDidMount = this.componentDidMount.bind(this)
    }
    componentDidMount() {
        var url = "https://api.iextrading.com/1.0/stock/" + this.state.ticker + "/peers"
        axios.get(url)
            .then(res => {
                var data = res.data 
                for(var i = 0; i != data.length; i++) {
                    var peerTicker = data[i]
                    var json_dict = {ticker: data[i]}
                    this.setState({peers: [...this.state.peers, <PeerComponent ticker={peerTicker} /> ]})                    
                }
            })
    }
    render() {
        const peerItems = this.state.peers.map((peer_ticker) =>
            <div className="shadow-lg card mb-3">
                {peer_ticker}  
            </div>
        )
        return (
            <div>
                {peerItems}
            </div>
        )
    }
}
export default Peers;