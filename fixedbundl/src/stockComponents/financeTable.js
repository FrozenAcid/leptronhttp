import React, { Component } from 'react';
import axios from 'axios';
import {KJUR} from 'jsrsasign';
import {Dispatcher} from 'flux';
import {Line} from "react-chartjs-2";


class Finance extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ticker: this.props.ticker,
            q1: {
                date: null,
                grossProfit: null,
                costOfRevenue: null
            },
            q2: {
                date: null,
                grossProfit: null,
                costOfRevenue: null
            },
            q3: {
                date: null,
                grossProfit: null,
                costOfRevenue: null
            },
            q4: {
                date: null,
                grossProfit: null,
                costOfRevenue: null
            }
        }
    }
    componentDidMount() {
        var url = "https://api.iextrading.com/1.0/stock/" + this.state.ticker + "/financials"
        axios.get(url)
            .then(res => {
                var data = res.data.financials

                var q1Data = data[0]
                var q2Data = data[1]
                var q3Data = data[2]
                var q4Data = data[3]
                this.setState({q1: {grossProfit: q1Data.grossProfit, date: q1Data.reportDate, costOfRevenue: q1Data.costOfRevenue, operatingRevenue: q1Data.operatingRevenue, totalRevenue: q1Data.totalRevenue, operatingIncome: q1Data.operatingIncome,netIncome: q1Data.netIncome,
                    researchAndDevelopment: q1Data.researchAndDevelopment, operatingExpense: q1Data.operatingExpense, currentAssets: q1Data.currentAssets,totalAssets: q1Data.totalAssets, totalLiabilities: q1Data.totalLiabilities, currentCash: q1Data.currentCash,
                        currentDebt: q1Data.currentDebt,totalCash:q1Data.totalCash, totalDebt: q1Data.totalDebt, shareholderEquity:q1Data.shareholderEquity,cashChange:q1Data.cashChange,cashFlow:q1Data.cashFlow}, 
                    q2: {grossProfit: q2Data.grossProfit, date: q2Data.reportDate, costOfRevenue: q2Data.costOfRevenue, operatingRevenue: q2Data.operatingRevenue, totalRevenue: q2Data.totalRevenue, operatingIncome: q2Data.operatingIncome,netIncome: q2Data.netIncome,
                        researchAndDevelopment: q2Data.researchAndDevelopment, operatingExpense: q2Data.operatingExpense, currentAssets: q2Data.currentAssets,totalAssets: q2Data.totalAssets, totalLiabilities: q2Data.totalLiabilities, currentCash: q2Data.currentCash,
                            currentDebt: q2Data.currentDebt, totalCash:q2Data.totalCash,totalDebt: q2Data.totalDebt,shareholderEquity:q2Data.shareholderEquity,cashChange:q2Data.cashChange,cashFlow:q2Data.cashFlow}, 
                    q3: {grossProfit: q3Data.grossProfit, date: q3Data.reportDate, costOfRevenue: q3Data.costOfRevenue, operatingRevenue: q3Data.operatingRevenue, totalRevenue: q3Data.totalRevenue, operatingIncome: q3Data.operatingIncome,netIncome: q3Data.netIncome,
                        researchAndDevelopment: q3Data.researchAndDevelopment, operatingExpense: q3Data.operatingExpense, currentAssets: q3Data.currentAssets,totalAssets: q3Data.totalAssets, totalLiabilities: q3Data.totalLiabilities, currentCash: q3Data.currentCash,
                            currentDebt: q3Data.currentDebt,totalCash:q3Data.totalCash,totalDebt: q1Data.totalDebt,shareholderEquity:q3Data.shareholderEquity,cashChange:q3Data.cashChange,cashFlow:q3Data.cashFlow}, 
                    q4: {grossProfit: q4Data.grossProfit, date: q4Data.reportDate, costOfRevenue: q3Data.costOfRevenue, operatingRevenue: q4Data.operatingRevenue, totalRevenue: q4Data.totalRevenue, operatingIncome: q4Data.operatingIncome,netIncome: q4Data.netIncome,
                        researchAndDevelopment: q4Data.researchAndDevelopment, operatingExpense: q4Data.operatingExpense, currentAssets: q4Data.currentAssets,totalAssets: q4Data.totalAssets, totalLiabilities: q4Data.totalLiabilities, currentCash: q4Data.currentCash,
                            currentDebt: q4Data.currentDebt,totalCash:q4Data.totalCash,totalDebt: q4Data.totalDebt,shareholderEquity:q4Data.shareholderEquity,cashChange:q4Data.cashChange,cashFlow:q4Data.cashFlow}})
            })
    }
    render() {
        return (
            <div className="table-responsive">
                <table className="table table-striped table-light">
                    <thead>
                        <th scope="col"></th>
                        <th scope="col">{this.state.q1.date}</th>
                        <th scope="col">{this.state.q2.date}</th>
                        <th scope="col">{this.state.q3.date}</th>
                        <th scope="col">{this.state.q4.date}</th>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Gross Profit</th>
                            <td>{this.state.q1.grossProfit}</td>
                            <td>{this.state.q2.grossProfit}</td>
                            <td>{this.state.q3.grossProfit}</td>
                            <td>{this.state.q4.grossProfit}</td>
                        </tr>
                        <tr>
                            <th scope="row">Cost of Revenue</th>
                            <td>{this.state.q1.costOfRevenue}</td>
                            <td>{this.state.q2.costOfRevenue}</td>
                            <td>{this.state.q3.costOfRevenue}</td>
                            <td>{this.state.q4.costOfRevenue}</td>
                        </tr>
                        <tr>
                            <th scope="row">Operating Revenue</th>
                            <td>{this.state.q1.operatingRevenue}</td>
                            <td>{this.state.q2.operatingRevenue}</td>
                            <td>{this.state.q3.operatingRevenue}</td>
                            <td>{this.state.q4.operatingRevenue }</td>
                        </tr>
                        <tr>
                            <th scope="row">Total Revenue</th>
                            <td>{this.state.q1.totalRevenue}</td>
                            <td>{this.state.q2.totalRevenue}</td>
                            <td>{this.state.q3.totalRevenue}</td>
                            <td>{this.state.q4.totalRevenue }</td>
                        </tr>
                        <tr>
                            <th scope="row">Operating Income</th>
                            <td>{this.state.q1.operatingIncome}</td>
                            <td>{this.state.q2.operatingIncome}</td>
                            <td>{this.state.q3.operatingIncome}</td>
                            <td>{this.state.q4.operatingIncome }</td>
                        </tr>
                        <tr>
                            <th scope="row">Net Income</th>
                            <td>{this.state.q1.netIncome}</td>
                            <td>{this.state.q2.netIncome}</td>
                            <td>{this.state.q3.netIncome}</td>
                            <td>{this.state.q4.netIncome }</td>
                        </tr>
                        <tr>
                            <th scope="row">Research and Development</th>
                            <td>{this.state.q1.researchAndDevelopment}</td>
                            <td>{this.state.q2.researchAndDevelopment}</td>
                            <td>{this.state.q3.researchAndDevelopment}</td>
                            <td>{this.state.q4.researchAndDevelopment }</td>
                        </tr>
                        <tr>
                            <th scope="row">Operating Expenses</th>
                            <td>{this.state.q1.operatingExpense}</td>
                            <td>{this.state.q2.operatingExpense}</td>
                            <td>{this.state.q3.operatingExpense}</td>
                            <td>{this.state.q4.operatingExpense }</td>
                        </tr>
                        <tr>
                            <th scope="row">Current Assets</th>
                            <td>{this.state.q1.currentAssets}</td>
                            <td>{this.state.q2.currentAssets}</td>
                            <td>{this.state.q3.currentAssets}</td>
                            <td>{this.state.q4.currentAssets }</td>
                        </tr>
                        <tr>
                            <th scope="row">Total Assets</th>
                            <td>{this.state.q1.totalAssets}</td>
                            <td>{this.state.q2.totalAssets}</td>
                            <td>{this.state.q3.totalAssets}</td>
                            <td>{this.state.q4.totalAssets }</td>
                        </tr>
                        <tr>
                            <th scope="row">Total Liabilities</th>
                            <td>{this.state.q1.totalLiabilities}</td>
                            <td>{this.state.q2.totalLiabilities}</td>
                            <td>{this.state.q3.totalLiabilities}</td>
                            <td>{this.state.q4.totalLiabilities }</td>
                        </tr>
                        <tr>
                            <th scope="row">Current Cash</th>
                            <td>{this.state.q1.currentCash}</td>
                            <td>{this.state.q2.currentCash}</td>
                            <td>{this.state.q3.currentCash}</td>
                            <td>{this.state.q4.currentCash }</td>
                        </tr>
                        <tr>
                            <th scope="row">Current Debt</th>
                            <td>{this.state.q1.currentDebt}</td>
                            <td>{this.state.q2.currentDebt}</td>
                            <td>{this.state.q3.currentDebt}</td>
                            <td>{this.state.q4.currentDebt }</td>
                        </tr> 
                        <tr>
                            <th scope="row">Total Cash</th>
                            <td>{this.state.q1.totalCash}</td>
                            <td>{this.state.q2.totalCash}</td>
                            <td>{this.state.q3.totalCash}</td>
                            <td>{this.state.q4.totalCash }</td>
                        </tr>
                        <tr>
                            <th scope="row">Total Debt</th>
                            <td>{this.state.q1.totalDebt}</td>
                            <td>{this.state.q2.totalDebt}</td>
                            <td>{this.state.q3.totalDebt}</td>
                            <td>{this.state.q4.totalDebt }</td>
                        </tr>
                        <tr>
                            <th scope="row">Shareholder Equity</th>
                            <td>{this.state.q1.shareholderEquity}</td>
                            <td>{this.state.q2.shareholderEquity}</td>
                            <td>{this.state.q3.shareholderEquity}</td>
                            <td>{this.state.q4.shareholderEquity }</td>
                        </tr>
                        <tr>
                            <th scope="row">Cash Change</th>
                            <td>{this.state.q1.cashChange}</td>
                            <td>{this.state.q2.cashChange}</td>
                            <td>{this.state.q3.cashChange}</td>
                            <td>{this.state.q4.cashChange }</td>
                        </tr>
                        <tr>
                            <th scope="row">Cash Flow</th>
                            <td>{this.state.q1.cashFlow}</td>
                            <td>{this.state.q2.cashFlow}</td>
                            <td>{this.state.q3.cashFlow}</td>
                            <td>{this.state.q4.cashFlow }</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}
export default Finance;