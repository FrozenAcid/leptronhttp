import React, { Component } from 'react';
import axios from 'axios';
import {KJUR} from 'jsrsasign';
import {Dispatcher} from 'flux';
import {Line} from "react-chartjs-2";
class StockChart extends Component {
    constructor(props) {
        super(props) 
        this.state = {
            ticker: this.props.ticker,
            labels: [],
            price: [],
            chartTag: null
        }
        this.componentWillMount = this.componentWillMount.bind(this)
    }
    componentDidMount() {
        console.log("mounted")
    }
    componentWillMount() {
        const url = "https://api.iextrading.com/1.0/stock/" + this.state.ticker + "/chart/1d"
        axios.get(url)
            .then(res => {
                var data = res.data
                for(var i = 0; i != data.length; i++) {
                    const dataPoint = data[i]
                    this.setState({labels:[...this.state.labels, dataPoint.label],
                                  price: [...this.state.price,dataPoint.marketAverage]})
                }
                const data = (canvas) => {
                    const ctx = canvas.getContext("2d")
                    var t = ctx.createLinearGradient(0,500, 0, 300);
                    t.addColorStop(0, "#727cf5")
                    t.addColorStop(1, "#727cf5")
                    return {
                        labels: this.state.labels,
                        datasets:[{
                            data: this.state.price,
                            label: this.state.ticker + " - Stock Price",
                            backgroundColor: t,
                            borderColor: "#26ffc3",
                            hoverBorderColor: t, 
                            hoverBackgroundColor: t,
                            boderWidth: "1",
                            lineTension: "0"
                        }]
                    }
                }
                const options = {
                    chartArea: {
                        backgroundColor: 'rgba(251, 85, 85, 0.4)'
                    },
                    elements: {
                        point: {
                            radius: 0
                        }
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    hover: {
                        mode: 'index',
                        intersect: false
                     },
                    maintainAspectRatio: false,
                    scales: {
                        xAxes: [{
                            stacked: true,
                            gridLines: {
                                display:false
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                display:false
                            }   
                        }]
                    }
                }
                this.setState({chartTag: <Line data={data} options={options} height={"375"} />})
            })
    }
    render() {
        return (
            <div>
                {this.state.chartTag}
            </div>
        )
    }
}
export default StockChart;